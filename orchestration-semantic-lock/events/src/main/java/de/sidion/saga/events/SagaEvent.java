package de.sidion.saga.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class SagaEvent {

    private OrchestrationEventName eventName;
    private Integer bookingId;
    private String correlationId;
    private Long executionTime;
}
