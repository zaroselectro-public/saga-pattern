package de.sidion.saga.events;

public enum SagaStatus {
    NEW, PENDING, VERIFIED, COMPLETED, CANCELED
}
