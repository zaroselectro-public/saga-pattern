package de.sidion.saga.holidaybookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.holidaybookingservice.entities.BookingEntitiy;
import de.sidion.saga.holidaybookingservice.persistence.BookingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.UUID;

@Service
@Slf4j
public class BookingSagaOrchestratorService {

    private final BookingRepository bookingRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public BookingSagaOrchestratorService(BookingRepository bookingRepository,
                                          KafkaTemplate<String, Object> kafkaTemplate) {
        this.bookingRepository = bookingRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    @KafkaListener(topics = "saga-reply-orchestration", containerFactory = "sagaReplyEventKafkaListenerContainerBatchFactory")
    public void listenToBookings(@Payload final SagaEvent sagaEvent,
                                 @Header(KafkaHeaders.RECEIVED_TOPIC) String topic,
                                 @Header("sagaFrom") String sagaFrom,
                                 @Header("sagaTo") String sagaTo,
                                 @Header("badSagaStatus") String badSagaStatus) {
        log.info("incomming sagaEvent: {}", sagaEvent);
        final OrchestrationEventName replyEventName = sagaEvent.getEventName();
        if (replyEventName.equals(OrchestrationEventName.CARD_VERIFIED)) {
            applyFlightBooking(sagaEvent);
        }
        if (replyEventName.equals(OrchestrationEventName.FLIGHT_BOOKING_APPLIED)) {
            applyHotelBooking(sagaEvent);
        }
        if (replyEventName.equals(OrchestrationEventName.HOTEL_BOOKING_APPLIED)) {
            debitCard(sagaEvent);
        }
        if (replyEventName.equals(OrchestrationEventName.DEBIT_APPLIED)) {
            applyConfirmation(sagaEvent);
        }
        if (replyEventName.equals(OrchestrationEventName.CONFIRMATION_APPLIED)) {
            finishBooking(sagaEvent);
        }

        if (replyEventName.equals(OrchestrationEventName.CARD_VERIFICATION_FAILED)
                || replyEventName.equals(OrchestrationEventName.FLIGHT_BOOKING_FAILED)
                || replyEventName.equals(OrchestrationEventName.HOTEL_BOOKING_FAILED)
                || replyEventName.equals(OrchestrationEventName.DEBIT_CARD_FAILED)) {
            handleRejectBooking(sagaEvent);
        }
        if (replyEventName.equals(OrchestrationEventName.BAD_SAGA_STATUS)) {
            handleBadSagaStatus(sagaEvent, topic, sagaFrom, sagaTo, badSagaStatus);
        }

    }

    public void sendBooking(BookingEvent bookingEvent) {

        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send("bookings-orchestration", String.valueOf(bookingEvent.getBookingId()), bookingEvent);

        listenableFuture.addCallback(new ListenableFutureCallback<>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("send bookingEvent to topic: {}", "bookings-orchestration");
            }
        });

    }

    public SagaEvent startCreateSaga(final Integer bookingId) {
        final String correlationId = UUID.randomUUID().toString();
        final SagaEvent sagaEvent = SagaEvent.builder()
                .bookingId(bookingId)
                .eventName(OrchestrationEventName.VERIFY_CARD)
                .correlationId(correlationId)
                .executionTime(System.currentTimeMillis())
                .build();
        sendSagaEvent(sagaEvent, "payments-orchestration", "PaymentService");

        return sagaEvent;
    }

    private void finishBooking(final SagaEvent incommingSagaEvent) {
        final BookingEntitiy bookingEntitiy = bookingRepository.findByBookingId(incommingSagaEvent.getBookingId());
        bookingEntitiy.setBookingStatus("COMPLETED");

        bookingRepository.save(bookingEntitiy);

        releaseLocks(incommingSagaEvent);
    }


    private void handleRejectBooking(final SagaEvent sagaEvent) {
        log.info("reject Booking with id {} because of {}", sagaEvent.getBookingId(), sagaEvent.getEventName());

        final BookingEntitiy bookingEntitiy = bookingRepository.findByBookingId(sagaEvent.getBookingId());
        bookingEntitiy.setBookingStatus("CANCELED");

        bookingRepository.save(bookingEntitiy);

        rejectFlightBooking(sagaEvent);
        rejectHotelBooking(sagaEvent);
    }

    private void releaseLocks(final SagaEvent incommingSagaEvent) {

        // release flight booking
        final SagaEvent sagaEventReleaseFlightBooking = SagaEvent.builder()
                .bookingId(incommingSagaEvent.getBookingId())
                .eventName(OrchestrationEventName.RELEASE_FLIGHT_BOOKING)
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis())
                .build();
        sendSagaEvent(sagaEventReleaseFlightBooking, "flight-bookings-orchestration", "FlightBookingService");

        // release hotel booking
        final SagaEvent sagaEventReleaseHotelBooking = SagaEvent.builder()
                .bookingId(incommingSagaEvent.getBookingId())
                .eventName(OrchestrationEventName.RELEASE_HOTEL_BOOKING)
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis())
                .build();
        sendSagaEvent(sagaEventReleaseHotelBooking, "hotel-bookings-orchestration", "HotelBookingService");

        // release payment
        final SagaEvent sagaEventReleasePayment = SagaEvent.builder()
                .bookingId(incommingSagaEvent.getBookingId())
                .eventName(OrchestrationEventName.RELEASE_PAYMENT)
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis())
                .build();
        sendSagaEvent(sagaEventReleasePayment, "payments-orchestration", "PaymentService");

    }

    private void debitCard(final SagaEvent incommingSagaEvent) {
        final SagaEvent sagaEvent = SagaEvent.builder()
                .bookingId(incommingSagaEvent.getBookingId())
                .eventName(OrchestrationEventName.DEBIT_CARD)
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis())
                .build();
        sendSagaEvent(sagaEvent, "payments-orchestration", "PaymentService");
    }

    private void applyFlightBooking(final SagaEvent incommingSagaEvent) {
        final SagaEvent sagaEvent = SagaEvent.builder()
                .bookingId(incommingSagaEvent.getBookingId())
                .eventName(OrchestrationEventName.APPLY_FLIGHT_BOOKING)
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis())
                .build();
        sendSagaEvent(sagaEvent, "flight-bookings-orchestration", "FlightBookingService");
    }

    private void applyHotelBooking(final SagaEvent incommingSagaEvent) {
        final SagaEvent sagaEvent = SagaEvent.builder()
                .bookingId(incommingSagaEvent.getBookingId())
                .eventName(OrchestrationEventName.APPLY_HOTEL_BOOKING)
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis())
                .build();
        sendSagaEvent(sagaEvent, "hotel-bookings-orchestration", "HotelBookingService");
    }

    private void applyConfirmation(final SagaEvent incommingSagaEvent) {
        final SagaEvent sagaEvent = SagaEvent.builder()
                .bookingId(incommingSagaEvent.getBookingId())
                .eventName(OrchestrationEventName.APPLY_CONFIRMATION)
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis())
                .build();
        sendSagaEvent(sagaEvent, "confirmation-orchestration", "ConfirmationService");
    }

    private void rejectFlightBooking(final SagaEvent incommingSagaEvent) {
        final SagaEvent sagaEvent = SagaEvent.builder()
                .bookingId(incommingSagaEvent.getBookingId())
                .eventName(OrchestrationEventName.REJECT_FLIGHT_BOOKING)
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis())
                .build();
        sendSagaEvent(sagaEvent, "flight-bookings-orchestration", "FlightBookingService");
    }

    private void rejectHotelBooking(final SagaEvent incommingSagaEvent) {
        final SagaEvent sagaEvent = SagaEvent.builder()
                .bookingId(incommingSagaEvent.getBookingId())
                .eventName(OrchestrationEventName.REJECT_HOTEL_BOOKING)
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis())
                .build();
        sendSagaEvent(sagaEvent, "hotel-bookings-orchestration", "HotelBookingService");
    }

    private void sendSagaEvent(SagaEvent sagaEvent, String topic, String recievingService) {
        final Message<SagaEvent> message = MessageBuilder
                .withPayload(sagaEvent)
                .setHeader(KafkaHeaders.TOPIC, topic)
                .setHeader(KafkaHeaders.MESSAGE_KEY, String.valueOf(sagaEvent.getBookingId()))
                .setHeader("sagaFrom", "Orchestrator")
                .setHeader("sagaTo", recievingService)
                .build();

        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send(message);

        listenableFuture.addCallback(new ListenableFutureCallback<>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("send sagaEvent with status {} to topic: {}", sagaEvent.getEventName(), topic);
            }
        });
    }

    private void handleBadSagaStatus(final SagaEvent sagaEvent, String topic, String sagaFrom, String sagaTo, String badSagaStatus) {
        log.warn("Saga with correlationId {} is stopping", sagaEvent.getCorrelationId());
        log.warn("sagaFrom: {}", sagaFrom);
        log.warn("badSagaStatus: {}", badSagaStatus);

        //TODO: implement retry mechanism
    }

}
