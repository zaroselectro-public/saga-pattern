package de.sidion.saga.holidaybookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.holidaybookingservice.entities.BookingEntitiy;
import de.sidion.saga.holidaybookingservice.persistence.BookingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BookingService {

    private final BookingRepository bookingRepository;
    private final BookingEventToBookingEntityMapper bookingEventToBookingEntityMapper;
    private final BookingSagaOrchestratorService bookingSagaOrchestratorService;


    public BookingService(BookingRepository bookingRepository,
                          BookingEventToBookingEntityMapper bookingEventToBookingEntityMapper, BookingSagaOrchestratorService bookingSagaOrchestratorService) {
        this.bookingRepository = bookingRepository;
        this.bookingEventToBookingEntityMapper = bookingEventToBookingEntityMapper;
        this.bookingSagaOrchestratorService = bookingSagaOrchestratorService;
    }

    public SagaEvent createBooking(final BookingEvent bookingEvent) {
        final BookingEntitiy bookingEntitiy = persistBooking(bookingEvent);
        bookingEvent.setBookingId(bookingEntitiy.getBookingId());
        bookingSagaOrchestratorService.sendBooking(bookingEvent);
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return bookingSagaOrchestratorService.startCreateSaga(bookingEntitiy.getBookingId());
    }

    private BookingEntitiy persistBooking(BookingEvent booking) {
        final BookingEntitiy bookingEntitiy = bookingEventToBookingEntityMapper.createEntityFromEvent(booking, "PENDING");

        return bookingRepository.save(bookingEntitiy);
    }


}
