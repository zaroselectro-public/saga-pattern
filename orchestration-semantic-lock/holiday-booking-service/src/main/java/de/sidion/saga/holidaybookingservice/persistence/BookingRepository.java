package de.sidion.saga.holidaybookingservice.persistence;

import de.sidion.saga.holidaybookingservice.entities.BookingEntitiy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<BookingEntitiy, Integer> {

    BookingEntitiy findByBookingId(Integer bookingId);
}
