package de.sidion.saga.tracingservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tracings")
@Entity
public class TracingEntitiy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String eventName;
    private Integer bookingId;
    private Long executionTime;
    private String correlationId;
    private String topic;
    private String sagaFrom;
    private String sagaTo;

}
