package de.sidion.saga.tracingservice.services;

import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.tracingservice.entities.TracingEntitiy;
import de.sidion.saga.tracingservice.frontend.TracingModel;
import de.sidion.saga.tracingservice.persistence.TracingRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TracingService {

    private final TracingRepository tracingRepository;

    public TracingService(TracingRepository tracingRepository) {
        this.tracingRepository = tracingRepository;
    }


    public void createTracing(final SagaEvent sagaEvent, final String topic, String sagaFrom, String sagaTo) {
        final TracingEntitiy tracingEntitiy = TracingEntitiy.builder()
                .bookingId(sagaEvent.getBookingId())
                .correlationId(sagaEvent.getCorrelationId())
                .executionTime(sagaEvent.getExecutionTime())
                .eventName(sagaEvent.getEventName().toString())
                .topic(topic)
                .sagaFrom(sagaFrom)
                .sagaTo(sagaTo)
                .build();

        tracingRepository.save(tracingEntitiy);
    }

    public List<TracingModel> getTracingsByCorrelationId(final String correlationId) {

        final List<TracingEntitiy> tracings = tracingRepository.findTracingEntitiyByCorrelationIdOrderByExecutionTimeAsc(correlationId);
        if (!tracings.isEmpty()) {
            return tracings.stream().map(tr -> convertEntityToModel(tr)).collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    private TracingModel convertEntityToModel(TracingEntitiy tr) {
        return TracingModel.builder()
                .bookingId(tr.getBookingId())
                .correlationId(tr.getCorrelationId())
                .executionTime(tr.getExecutionTime())
                .eventName(tr.getEventName())
                .topic(tr.getTopic())
                .sagaFrom(tr.getSagaFrom())
                .sagaTo(tr.getSagaTo())
                .build();
    }

}
