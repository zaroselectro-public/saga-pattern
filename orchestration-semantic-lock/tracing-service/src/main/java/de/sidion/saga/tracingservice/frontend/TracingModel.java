package de.sidion.saga.tracingservice.frontend;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class TracingModel {

    private String eventName;
    private Integer bookingId;
    private Long executionTime;
    private String correlationId;
    private String topic;
    private String sagaFrom;
    private String sagaTo;

}
