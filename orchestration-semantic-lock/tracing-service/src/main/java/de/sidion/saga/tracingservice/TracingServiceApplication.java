package de.sidion.saga.tracingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TracingServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TracingServiceApplication.class, args);
    }

}
