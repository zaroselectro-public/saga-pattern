package de.sidion.saga.flightbookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.SagaEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FlightBookingKafkaConsumer {

    private final FlightBookingService flightBookingService;

    public FlightBookingKafkaConsumer(FlightBookingService flightBookingService) {
        this.flightBookingService = flightBookingService;
    }

    @KafkaListener(topics = "bookings-orchestration", containerFactory = "bookingEventKafkaListenerContainerBatchFactory")
    public void listenToBookings(@Payload final BookingEvent bookingEvent) {
        log.info("incomming bookingEvent: {}", bookingEvent);
        flightBookingService.createFlightFromBooking(bookingEvent);
    }

    @KafkaListener(topics = "flight-bookings-orchestration", containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToFlightBookings(@Payload final SagaEvent sagaEvent) {
        log.info("incomming sagaEvent: {}", sagaEvent);
        if (sagaEvent.getEventName().equals(OrchestrationEventName.APPLY_FLIGHT_BOOKING)) {
            flightBookingService.applyFlightBooking(sagaEvent);
        } else if (sagaEvent.getEventName().equals(OrchestrationEventName.RELEASE_FLIGHT_BOOKING)) {
            flightBookingService.releaseFlightBooking(sagaEvent);
        } else if (sagaEvent.getEventName().equals(OrchestrationEventName.REJECT_FLIGHT_BOOKING)) {
            log.warn("cancel booking with id {}", sagaEvent.getBookingId());
            flightBookingService.rejectBooking(sagaEvent);
        }
    }

}
