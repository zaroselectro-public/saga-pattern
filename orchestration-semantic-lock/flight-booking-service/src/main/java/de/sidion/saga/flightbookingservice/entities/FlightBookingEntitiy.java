package de.sidion.saga.flightbookingservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "flightbookings")
@Entity
public class FlightBookingEntitiy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer flightBookingId;
    private Integer bookingId;
    private Integer customerId;

    private String outgoingFlightId;
    private LocalDate outgoingFlightDate;
    private String returnFlightId;
    private LocalDate returnFlightDate;

    private String sagaStatus;

}
