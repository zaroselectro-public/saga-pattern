package de.sidion.saga.flightbookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.events.SagaStatus;
import de.sidion.saga.flightbookingservice.entities.FlightBookingEntitiy;
import de.sidion.saga.flightbookingservice.persistence.FlightBookingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class FlightBookingService {

    private final FlightBookingRepository flightBookingRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public FlightBookingService(FlightBookingRepository flightBookingRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.flightBookingRepository = flightBookingRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void createFlightFromBooking(final BookingEvent bookingEvent) {
        final FlightBookingEntitiy flightBookingEntitiy = FlightBookingEntitiy.builder()
                .bookingId(bookingEvent.getBookingId())
                .customerId(bookingEvent.getCustomerEvent().getCustomerId())
                .outgoingFlightId(bookingEvent.getFlightEvent().getOutgoingFlightId())
                .outgoingFlightDate(bookingEvent.getFlightEvent().getOutgoingFlightDate())
                .returnFlightId(bookingEvent.getFlightEvent().getReturnFlightId())
                .returnFlightDate(bookingEvent.getFlightEvent().getReturnFlightDate())
                .sagaStatus(SagaStatus.NEW.toString())
                .build();

        flightBookingRepository.save(flightBookingEntitiy);

    }

    public void releaseFlightBooking(final SagaEvent incommingSagaEvent) {
        final FlightBookingEntitiy flightBookingEntitiy = flightBookingRepository.findByBookingId(incommingSagaEvent.getBookingId());
        if (!flightBookingEntitiy.getSagaStatus().equals(SagaStatus.PENDING.toString())) {
            handleBadSagaStatus(incommingSagaEvent, "releaseFlightBooking");
            return;
        }
        log.info("release flight booking for bookingId {}", incommingSagaEvent.getBookingId());
        flightBookingEntitiy.setSagaStatus(SagaStatus.COMPLETED.toString());
        flightBookingRepository.save(flightBookingEntitiy);
    }

    public void applyFlightBooking(final SagaEvent incommingSagaEvent) {
        final FlightBookingEntitiy flightBookingEntitiy = flightBookingRepository.findByBookingId(incommingSagaEvent.getBookingId());

        if (!flightBookingEntitiy.getSagaStatus().equals(SagaStatus.NEW.toString())) {
            handleBadSagaStatus(incommingSagaEvent, "applyFlightBooking");
            return;
        }

        final SagaEvent.SagaEventBuilder builder = SagaEvent.builder();
        builder.bookingId(incommingSagaEvent.getBookingId())
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis());


        if (isValidFlightBooking(flightBookingEntitiy)) {
            log.info("apply flight booking for bookingId {}", incommingSagaEvent.getBookingId());
            builder.eventName(OrchestrationEventName.FLIGHT_BOOKING_APPLIED);
            flightBookingEntitiy.setSagaStatus(SagaStatus.PENDING.toString());
            flightBookingRepository.save(flightBookingEntitiy);
        } else {
            builder.eventName(OrchestrationEventName.FLIGHT_BOOKING_FAILED);
        }

        sendSagaMessage(createSagaMessage(builder.build(), ""));
    }

    public void rejectBooking(final SagaEvent incommingSagaEvent) {
        final FlightBookingEntitiy flightBookingEntitiyToDelete = flightBookingRepository.findByBookingId(incommingSagaEvent.getBookingId());
        if (flightBookingEntitiyToDelete != null) {
            log.info("DELETE flightBookingEntitiy: {}", flightBookingEntitiyToDelete);
            flightBookingRepository.delete(flightBookingEntitiyToDelete);
        }
    }

    private void handleBadSagaStatus(SagaEvent incommingSagaEvent, String badSagaStatus) {
        final SagaEvent.SagaEventBuilder builder = SagaEvent.builder();
        builder.bookingId(incommingSagaEvent.getBookingId())
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis());
        builder.eventName(OrchestrationEventName.BAD_SAGA_STATUS);

        sendSagaMessage(createSagaMessage(builder.build(), badSagaStatus));
    }


    private Message<SagaEvent> createSagaMessage(SagaEvent sagaEvent, String badSagaStatus) {
        return MessageBuilder
                .withPayload(sagaEvent)
                .setHeader(KafkaHeaders.TOPIC, "saga-reply-orchestration")
                .setHeader(KafkaHeaders.MESSAGE_KEY, String.valueOf(sagaEvent.getBookingId()))
                .setHeader("sagaFrom", "FlightBookingService")
                .setHeader("sagaTo", "Orchestrator")
                .setHeader("badSagaStatus", badSagaStatus)
                .build();

    }

    private void sendSagaMessage(Message<SagaEvent> message) {
        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send(message);
        listenableFuture.addCallback(new ListenableFutureCallback<>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("sending reply {}", message.getPayload());
            }
        });
    }


    private boolean isValidFlightBooking(final FlightBookingEntitiy flightBookingEntitiy) {

        // we only fly out with Lufthansa
        return flightBookingEntitiy != null && flightBookingEntitiy.getOutgoingFlightId().startsWith("LH");
    }

}
