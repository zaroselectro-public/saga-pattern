package de.sidion.saga.confirmationservice.services;

import de.sidion.saga.confirmationservice.entities.ConfirmationEntitiy;
import de.sidion.saga.confirmationservice.persistence.ConfirmationRepository;
import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.SagaEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class ConfirmationService {

    private final ConfirmationRepository confirmationRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public ConfirmationService(ConfirmationRepository confirmationRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.confirmationRepository = confirmationRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void createConfirmationFromBooking(final BookingEvent bookingEvent) {
        final ConfirmationEntitiy confirmationEntitiy = ConfirmationEntitiy.builder()
                .bookingId(bookingEvent.getBookingId())
                .customerId(bookingEvent.getCustomerEvent().getCustomerId())
                .customerName(bookingEvent.getCustomerEvent().getName())
                .email(bookingEvent.getCustomerEvent().getEmail())
                .build();

        confirmationRepository.save(confirmationEntitiy);

    }


    public void sendConfirmationEmail(final SagaEvent incommingSagaEvent) {
        final ConfirmationEntitiy confirmationEntitiy = confirmationRepository.findByBookingId(incommingSagaEvent.getBookingId());
        final SagaEvent.SagaEventBuilder builder = SagaEvent.builder();
        builder.bookingId(incommingSagaEvent.getBookingId())
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis());
        builder.eventName(OrchestrationEventName.CONFIRMATION_APPLIED);

        confirmationRepository.save(confirmationEntitiy);


        sendSagaEvent(builder.build());
    }

    private void sendSagaEvent(SagaEvent sagaEvent) {
        final Message<SagaEvent> message = MessageBuilder
                .withPayload(sagaEvent)
                .setHeader(KafkaHeaders.TOPIC, "saga-reply-orchestration")
                .setHeader(KafkaHeaders.MESSAGE_KEY, String.valueOf(sagaEvent.getBookingId()))
                .setHeader("sagaFrom", "ConfirmationService")
                .setHeader("sagaTo", "Orchestrator")
                .setHeader("badSagaStatus", "")
                .build();

        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send(message);
        listenableFuture.addCallback(new ListenableFutureCallback<>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("sending reply {}", sagaEvent);
            }
        });
    }

}
