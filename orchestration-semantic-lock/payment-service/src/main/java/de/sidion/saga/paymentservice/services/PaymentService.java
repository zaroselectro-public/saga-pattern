package de.sidion.saga.paymentservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.events.SagaStatus;
import de.sidion.saga.paymentservice.entities.PaymentEntitiy;
import de.sidion.saga.paymentservice.persistence.PaymentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class PaymentService {

    private final PaymentRepository paymentRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public PaymentService(PaymentRepository paymentRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.paymentRepository = paymentRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void createPaymentFromBooking(final BookingEvent bookingEvent) {
        final PaymentEntitiy carRentEntitiy = PaymentEntitiy.builder()
                .bookingId(bookingEvent.getBookingId())
                .customerId(bookingEvent.getCustomerEvent().getCustomerId())
                .price(bookingEvent.getPaymentEvent().getPrice())
                .cardNumber(bookingEvent.getPaymentEvent().getCardNumber())
                .sagaStatus(SagaStatus.NEW.toString())
                .build();

        paymentRepository.save(carRentEntitiy);

    }

    public void verifyCard(final SagaEvent incommingSagaEvent) {
        final PaymentEntitiy paymentEntitiy = paymentRepository.findByBookingId(incommingSagaEvent.getBookingId());
        if (!paymentEntitiy.getSagaStatus().equals(SagaStatus.NEW.toString())) {
            handleBadSagaStatus(incommingSagaEvent, "verifyCard");
            return;
        }

        final SagaEvent.SagaEventBuilder builder = SagaEvent.builder();
        builder.bookingId(incommingSagaEvent.getBookingId())
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis());

        if (isValidCardNumber(paymentEntitiy)) {
            log.info("Verify card for bookingId {}", incommingSagaEvent.getBookingId());
            builder.eventName(OrchestrationEventName.CARD_VERIFIED);
            paymentEntitiy.setSagaStatus(SagaStatus.PENDING.toString());
            paymentRepository.save(paymentEntitiy);
        } else {
            builder.eventName(OrchestrationEventName.CARD_VERIFICATION_FAILED);
            rejectBooking(incommingSagaEvent);
        }

        sendSagaMessage(createSagaMessage(builder.build(), ""));
    }

    public void debitCard(final SagaEvent incommingSagaEvent) {
        final PaymentEntitiy paymentEntitiy = paymentRepository.findByBookingId(incommingSagaEvent.getBookingId());
        if (!paymentEntitiy.getSagaStatus().equals(SagaStatus.PENDING.toString())) {
            handleBadSagaStatus(incommingSagaEvent, "debitCard");
            return;
        }

        final SagaEvent.SagaEventBuilder builder = SagaEvent.builder();
        builder.bookingId(incommingSagaEvent.getBookingId())
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis());

        if (isDebitSuccessful(paymentEntitiy)) {
            log.info("Debit card for bookingId {}", incommingSagaEvent.getBookingId());
            builder.eventName(OrchestrationEventName.DEBIT_APPLIED);
        } else {
            builder.eventName(OrchestrationEventName.DEBIT_CARD_FAILED);
            rejectBooking(incommingSagaEvent);
        }

        sendSagaMessage(createSagaMessage(builder.build(), ""));

    }

    public void rejectBooking(final SagaEvent incommingSagaEvent) {
        final PaymentEntitiy paymentEntitiyToDelete = paymentRepository.findByBookingId(incommingSagaEvent.getBookingId());

        if (paymentEntitiyToDelete != null) {
            log.info("DELETE paymentEntitiy: {}", paymentEntitiyToDelete);
            paymentRepository.delete(paymentEntitiyToDelete);
        }
    }

    public void releasePayment(final SagaEvent incommingSagaEvent) {
        final PaymentEntitiy paymentEntitiy = paymentRepository.findByBookingId(incommingSagaEvent.getBookingId());
        if (!paymentEntitiy.getSagaStatus().equals(SagaStatus.PENDING.toString())) {
            handleBadSagaStatus(incommingSagaEvent, "releasePayment");
            return;
        }
        log.info("release payment for bookingId {}", incommingSagaEvent.getBookingId());
        paymentEntitiy.setSagaStatus(SagaStatus.COMPLETED.toString());
        paymentRepository.save(paymentEntitiy);
    }


    private void handleBadSagaStatus(SagaEvent incommingSagaEvent, String badSagaStatus) {
        final SagaEvent.SagaEventBuilder builder = SagaEvent.builder();
        builder.bookingId(incommingSagaEvent.getBookingId())
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis());
        builder.eventName(OrchestrationEventName.BAD_SAGA_STATUS);

        sendSagaMessage(createSagaMessage(builder.build(), badSagaStatus));
    }


    private Message<SagaEvent> createSagaMessage(SagaEvent sagaEvent, String badSagaStatus) {
        return MessageBuilder
                .withPayload(sagaEvent)
                .setHeader(KafkaHeaders.TOPIC, "saga-reply-orchestration")
                .setHeader(KafkaHeaders.MESSAGE_KEY, String.valueOf(sagaEvent.getBookingId()))
                .setHeader("sagaFrom", "PaymentService")
                .setHeader("sagaTo", "Orchestrator")
                .setHeader("badSagaStatus", badSagaStatus)
                .build();

    }

    private void sendSagaMessage(Message<SagaEvent> message) {
        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send(message);
        listenableFuture.addCallback(new ListenableFutureCallback<>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("sending reply {}", message.getPayload());
            }
        });
    }

    private boolean isValidCardNumber(PaymentEntitiy paymentEntitiy) {
        final String cardNumber = paymentEntitiy.getCardNumber();
        if (cardNumber.length() != 15) {
            return false;
        }
        return cardNumber.startsWith("37") && !cardNumber.startsWith("34");
    }

    private boolean isDebitSuccessful(PaymentEntitiy paymentEntitiy) {
        return paymentEntitiy != null && !(paymentEntitiy.getCardNumber().equals("371234567891008") && paymentEntitiy.getPrice() > 1500);
    }

}
