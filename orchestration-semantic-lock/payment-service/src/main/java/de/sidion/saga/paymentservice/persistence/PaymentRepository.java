package de.sidion.saga.paymentservice.persistence;

import de.sidion.saga.paymentservice.entities.PaymentEntitiy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<PaymentEntitiy, Integer> {

    PaymentEntitiy findByBookingId(Integer bookingId);

}
