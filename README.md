# Mit Saga verteilte Transaktionen verwalten

In modernen Microservice-Architekturen mit eigener Datenhaltung besteht immer wieder das Problem der Datenkonsistenz bei Fehlerfällen innerhalb verteilter Transaktionen. Anstatt Distributed Transactions zu verwenden wird mittlerweile hauptsächlich auf die Verwendung des Saga-Patterns gesetzt.
In diesen Code-Beispielen möchte ich dazu ein paar praktische Beispiele für die Umsetzung bieten.


### Kontakt
thomas.mueller@sidion.de
