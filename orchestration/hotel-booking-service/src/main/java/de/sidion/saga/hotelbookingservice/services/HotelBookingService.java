package de.sidion.saga.hotelbookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.hotelbookingservice.entities.HotelBookingEntitiy;
import de.sidion.saga.hotelbookingservice.persistence.HotelBookingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class HotelBookingService {

    private final HotelBookingRepository hotelBookingRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;


    public HotelBookingService(HotelBookingRepository hotelBookingRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.hotelBookingRepository = hotelBookingRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void createHotelFromBooking(final BookingEvent bookingEvent) {
        final HotelBookingEntitiy hotelBookingEntitiy = HotelBookingEntitiy.builder()
                .bookingId(bookingEvent.getBookingId())
                .customerId(bookingEvent.getCustomerEvent().getCustomerId())
                .hotelId(bookingEvent.getHotelEvent().getHotelId())
                .hotelFromDate(bookingEvent.getHotelEvent().getFromDate())
                .hotelToDate(bookingEvent.getHotelEvent().getToDate())
                .bookingStatus("PENDING")
                .build();

        hotelBookingRepository.save(hotelBookingEntitiy);

    }

    public void handleUpdate(final SagaEvent incommingSagaEvent) {

        final HotelBookingEntitiy hotelBookingEntity = hotelBookingRepository.findByBookingIdAndBookingStatus(incommingSagaEvent.getBookingId(), "PENDING");
        final SagaEvent.SagaEventBuilder builder = SagaEvent.builder();
        builder.bookingId(incommingSagaEvent.getBookingId())
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis());

        if (isValidHotelBooking(hotelBookingEntity)) {
            builder.eventName(OrchestrationEventName.HOTEL_BOOKING_APPLIED);
            hotelBookingEntity.setBookingStatus("COMPLETED");
            hotelBookingRepository.save(hotelBookingEntity);
        } else {
            builder.eventName(OrchestrationEventName.HOTEL_BOOKING_FAILED);
        }

        sendSagaEvent(builder.build());
    }

    public void rejectBooking(final SagaEvent incommingSagaEvent) {
        final HotelBookingEntitiy hotelBookingEntitiyToDelete = hotelBookingRepository.findByBookingIdAndBookingStatus(incommingSagaEvent.getBookingId(), "PENDING");
        if (hotelBookingEntitiyToDelete != null) {
            log.info("DELETE hotelBookingEntitiy: {}", hotelBookingEntitiyToDelete);
            hotelBookingRepository.delete(hotelBookingEntitiyToDelete);
        }

    }

    private void sendSagaEvent(SagaEvent sagaEvent) {
        final Message<SagaEvent> message = MessageBuilder
                .withPayload(sagaEvent)
                .setHeader(KafkaHeaders.TOPIC, "saga-reply-orchestration")
                .setHeader(KafkaHeaders.MESSAGE_KEY, String.valueOf(sagaEvent.getBookingId()))
                .setHeader("sagaFrom", "HotelBookingService")
                .setHeader("sagaTo", "Orchestrator")
                .build();

        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send(message);
//        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send("saga-reply-orchestration", String.valueOf(sagaEvent.getBookingId()), sagaEvent);
        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("sending reply {}", sagaEvent);
            }
        });
    }


    private boolean isValidHotelBooking(final HotelBookingEntitiy hotelBookingEntity) {
        // only RIU hotels are valid
        return hotelBookingEntity != null && hotelBookingEntity.getHotelId().startsWith("RIU");
    }
}
