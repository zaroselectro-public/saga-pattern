package de.sidion.saga.hotelbookingservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "hotelbookings")
@Entity
public class HotelBookingEntitiy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer hotelBookingId;
    private Integer bookingId;
    private Integer customerId;

    private String hotelId;
    private LocalDate hotelFromDate;
    private LocalDate hotelToDate;

    private String bookingStatus;

}
