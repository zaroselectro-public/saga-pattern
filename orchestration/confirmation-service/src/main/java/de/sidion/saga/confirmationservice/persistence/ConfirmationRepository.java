package de.sidion.saga.confirmationservice.persistence;

import de.sidion.saga.confirmationservice.entities.ConfirmationEntitiy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConfirmationRepository extends JpaRepository<ConfirmationEntitiy, Integer> {

    ConfirmationEntitiy findByBookingId(Integer bookingId);
}
