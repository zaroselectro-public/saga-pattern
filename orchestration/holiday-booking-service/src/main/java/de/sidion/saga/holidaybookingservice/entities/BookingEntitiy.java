package de.sidion.saga.holidaybookingservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "bookings")
@Entity
public class BookingEntitiy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer bookingId;
    private String bookingStatus;
    private Integer customerId;
    private String customerName;
    private String email;

    private String outgoingFlightId;
    private LocalDate outgoingFlightDate;
    private String returnFlightId;
    private LocalDate returnFlightDate;

    private String hotelId;
    private LocalDate hotelFromDate;
    private LocalDate hotelToDate;

    private Integer price;
    private String cardNumber;


}
