package de.sidion.saga.holidaybookingservice.controller;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.holidaybookingservice.services.BookingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class BookingController {

    private final BookingService bookingService;

    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @PostMapping("/create")
    public SagaEvent createBooking(@RequestBody final BookingEvent bookingEvent) {
        return bookingService.createBooking(bookingEvent);
    }

}
