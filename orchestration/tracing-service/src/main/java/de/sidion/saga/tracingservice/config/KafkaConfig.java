package de.sidion.saga.tracingservice.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.sidion.saga.events.SagaEvent;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.Map;

@Configuration
@RequiredArgsConstructor
public class KafkaConfig {

    private final KafkaProperties kafkaProperties;
    private final ObjectMapper objectMapper;


    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, SagaEvent> sagaEventKafkaListenerContainerBatchFactory() {
        return createKafkaListenerContainerFactory(SagaEvent.class);
    }


    private <T> ConcurrentKafkaListenerContainerFactory<String, T> createKafkaListenerContainerFactory(final Class<T> type) {

        final ConcurrentKafkaListenerContainerFactory<String, T> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(createKafkaConsumerFactory(type));
        return factory;
    }

    private <T> ConsumerFactory<String, T> createKafkaConsumerFactory(final Class<T> type) {
        final Map<String, Object> consumerProperties = kafkaProperties.buildConsumerProperties();

        final StringDeserializer stringDeserializer = new StringDeserializer();
        stringDeserializer.configure(consumerProperties, true);

        final JsonDeserializer<T> jsonDeserializer = new JsonDeserializer<>(type, objectMapper);
        jsonDeserializer.configure(consumerProperties, false);

        return new DefaultKafkaConsumerFactory<>(consumerProperties,
                new ErrorHandlingDeserializer<>(stringDeserializer),
                new ErrorHandlingDeserializer<>(jsonDeserializer));
    }

}
