package de.sidion.saga.tracingservice.services;

import de.sidion.saga.events.SagaEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TracingKafkaConsumer {

    private final TracingService tracingService;

    public TracingKafkaConsumer(TracingService tracingService) {
        this.tracingService = tracingService;
    }


    @KafkaListener(topics = {"hotel-bookings-orchestration", "flight-bookings-orchestration", "confirmation-orchestration", "payments-orchestration", "saga-reply-orchestration"}, containerFactory = "sagaEventKafkaListenerContainerBatchFactory")
    public void listenToBookings(@Payload final SagaEvent sagaEvent,
                                 @Header(KafkaHeaders.RECEIVED_TOPIC) String topic,
                                 @Header("sagaFrom") String sagaFrom,
                                 @Header("sagaTo") String sagaTo) {
        log.info("incomming from topic {}, sagaEvent: {}", topic, sagaEvent);
        if (sagaEvent != null) {
            tracingService.createTracing(sagaEvent, topic, sagaFrom, sagaTo);
        }

    }


}
