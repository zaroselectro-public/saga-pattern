package de.sidion.saga.tracingservice.persistence;

import de.sidion.saga.tracingservice.entities.TracingEntitiy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TracingRepository extends JpaRepository<TracingEntitiy, Integer> {


    List<TracingEntitiy> findTracingEntitiyByCorrelationIdOrderByExecutionTimeAsc(String correlationId);

}
