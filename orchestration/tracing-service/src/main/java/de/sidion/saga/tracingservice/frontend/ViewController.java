package de.sidion.saga.tracingservice.frontend;

import de.sidion.saga.tracingservice.services.TracingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@Slf4j
public class ViewController {

    private final TracingService tracingService;

    public ViewController(TracingService tracingService) {
        this.tracingService = tracingService;
    }


    @GetMapping("/{correlationId}")
    public ModelAndView index(@PathVariable String correlationId) {
        final List<TracingModel> tracingsByCorrelationId = tracingService.getTracingsByCorrelationId(correlationId);
        final ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("tracings", tracingsByCorrelationId);

        return modelAndView;
    }

}
