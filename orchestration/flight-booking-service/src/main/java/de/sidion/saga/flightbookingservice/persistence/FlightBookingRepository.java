package de.sidion.saga.flightbookingservice.persistence;

import de.sidion.saga.flightbookingservice.entities.FlightBookingEntitiy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FlightBookingRepository extends JpaRepository<FlightBookingEntitiy, Integer> {

    FlightBookingEntitiy findByBookingId(Integer bookingId);
}
