package de.sidion.saga.flightbookingservice.services;

import de.sidion.saga.events.BookingEvent;
import de.sidion.saga.events.OrchestrationEventName;
import de.sidion.saga.events.SagaEvent;
import de.sidion.saga.flightbookingservice.entities.FlightBookingEntitiy;
import de.sidion.saga.flightbookingservice.persistence.FlightBookingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class FlightBookingService {

    private final FlightBookingRepository flightBookingRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public FlightBookingService(FlightBookingRepository flightBookingRepository, KafkaTemplate<String, Object> kafkaTemplate) {
        this.flightBookingRepository = flightBookingRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void createFlightFromBooking(final BookingEvent bookingEvent) {
        final FlightBookingEntitiy flightBookingEntitiy = FlightBookingEntitiy.builder()
                .bookingId(bookingEvent.getBookingId())
                .customerId(bookingEvent.getCustomerEvent().getCustomerId())
                .outgoingFlightId(bookingEvent.getFlightEvent().getOutgoingFlightId())
                .outgoingFlightDate(bookingEvent.getFlightEvent().getOutgoingFlightDate())
                .returnFlightId(bookingEvent.getFlightEvent().getReturnFlightId())
                .returnFlightDate(bookingEvent.getFlightEvent().getReturnFlightDate())
                .bookingStatus("PENDING")
                .build();

        flightBookingRepository.save(flightBookingEntitiy);

    }

    public void handleUpdate(final SagaEvent incommingSagaEvent) {
        final SagaEvent.SagaEventBuilder builder = SagaEvent.builder();
        builder.bookingId(incommingSagaEvent.getBookingId())
                .correlationId(incommingSagaEvent.getCorrelationId())
                .executionTime(System.currentTimeMillis());

        final FlightBookingEntitiy flightBookingEntitiy = flightBookingRepository.findByBookingId(incommingSagaEvent.getBookingId());

        if (isValidFlightBooking(flightBookingEntitiy)) {
            builder.eventName(OrchestrationEventName.FLIGHT_BOOKING_APPLIED);
            flightBookingEntitiy.setBookingStatus("COMPLETED");
            flightBookingRepository.save(flightBookingEntitiy);
        } else {
            builder.eventName(OrchestrationEventName.FLIGHT_BOOKING_FAILED);
        }

        sendSagaEvent(builder.build());

    }

    private void sendSagaEvent(SagaEvent sagaEvent) {
        final Message<SagaEvent> message = MessageBuilder
                .withPayload(sagaEvent)
                .setHeader(KafkaHeaders.TOPIC, "saga-reply-orchestration")
                .setHeader(KafkaHeaders.MESSAGE_KEY, String.valueOf(sagaEvent.getBookingId()))
                .setHeader("sagaFrom", "FlightBookingService")
                .setHeader("sagaTo", "Orchestrator")
                .build();

        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send(message);
//        final ListenableFuture<SendResult<String, Object>> listenableFuture = kafkaTemplate.send("saga-reply-orchestration", String.valueOf(sagaEvent.getBookingId()), sagaEvent);
        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error(ex.getMessage(), ex);
            }

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                log.info("sending reply {}", sagaEvent);
            }
        });
    }


    private boolean isValidFlightBooking(final FlightBookingEntitiy flightBookingEntitiy) {

        // we only fly out with Lufthansa
        return flightBookingEntitiy != null && flightBookingEntitiy.getOutgoingFlightId().startsWith("LH");
    }

    public void rejectBooking(final SagaEvent incommingSagaEvent) {
        final FlightBookingEntitiy flightBookingEntitiyToDelete = flightBookingRepository.findByBookingId(incommingSagaEvent.getBookingId());
        if (flightBookingEntitiyToDelete != null) {
            log.info("DELETE flightBookingEntitiy: {}", flightBookingEntitiyToDelete);
            flightBookingRepository.delete(flightBookingEntitiyToDelete);
        }
    }
}
