package de.sidion.saga.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class FlightEvent {

    private String outgoingFlightId;
    private LocalDate outgoingFlightDate;
    private String returnFlightId;
    private LocalDate returnFlightDate;
}
