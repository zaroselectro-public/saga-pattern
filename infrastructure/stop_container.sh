docker compose -f ./kafka/docker-compose.yml down

docker compose -f ./mysql/docker-compose-holiday-booking-service.yml down
docker compose -f ./mysql/docker-compose-flight-booking-service.yml down
docker compose -f ./mysql/docker-compose-hotel-booking-service.yml down
docker compose -f ./mysql/docker-compose-confirmation-service.yml down
docker compose -f ./mysql/docker-compose-payment-service.yml down
docker compose -f ./mysql/docker-compose-tracing-service.yml down


docker ps
