docker compose -f ./kafka/docker-compose.yml up -d

docker compose -f ./mysql/docker-compose-holiday-booking-service.yml up -d
docker compose -f ./mysql/docker-compose-flight-booking-service.yml up -d
docker compose -f ./mysql/docker-compose-hotel-booking-service.yml up -d
docker compose -f ./mysql/docker-compose-confirmation-service.yml up -d
docker compose -f ./mysql/docker-compose-payment-service.yml up -d
docker compose -f ./mysql/docker-compose-tracing-service.yml up -d

kafka-topics --create --topic bookings-orchestration --partitions 1 --replication-factor 1  --bootstrap-server localhost:9092
kafka-topics --create --topic flight-bookings-orchestration --partitions 1 --replication-factor 1  --bootstrap-server localhost:9092
kafka-topics --create --topic hotel-bookings-orchestration --partitions 1 --replication-factor 1  --bootstrap-server localhost:9092
kafka-topics --create --topic confirmation-orchestration --partitions 1 --replication-factor 1  --bootstrap-server localhost:9092
kafka-topics --create --topic payments-orchestration --partitions 1 --replication-factor 1  --bootstrap-server localhost:9092
kafka-topics --create --topic saga-reply-orchestration --partitions 1 --replication-factor 1  --bootstrap-server localhost:9092

docker ps
